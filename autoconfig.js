/**
 * This points Firefox at the configuration file, always located at the root of the Firefox install.
 */ 
pref("general.config.filename", "firefox.cfg");

/**
 * Firefox "byte-shifts" the configuration files by default, after the first run they wouldn't be 
 * readable. This disables the byte-shifting.
 */
pref("general.config.obscure_value", 0);

/**
 * A slightly unnecessary line but one I think that most people would appreciate.
 * This sets the default preference for `userchrome.css` to be enabled, meaning you can customise
 * Firefox using your own css.
 */
defaultPref("toolkit.legacyUserProfileCustomizations.stylesheets", true);