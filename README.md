# Firefox New Tab script

The script sets up `autoconfig.js` on Firefox to use a custom local new tab.

You can read more about this technique [here](https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig)
and [here](https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Enterprise_deployment_before_60)

The script uses enterprise configuration to overwrite the new tab page on your Firefox installation.
It adds two files (`autoconfig.js` and `firefox.cfg`) that execute code on startup.

You can find commented versions of both of these files in the repository.

- [Annotated autoconfig.js](autoconfig.js)
- [Annotated firefox.cfg](firefox.cfg)

## Usage

```bash
wget -O ff-set-startpage.sh https://stpg.tk/firefox-new-tab-script
bash ff-set-startpage.sh
```

You may need to run the script as `sudo` depending on your Firefox installation location.

## Compatibility

The script has been tested on a few different versions and OSs, but no extensive testing has taken
place yet. Further to that point, this part of Firefox code is not very well documented and it
prone to inconsistencies. Make an issue or join us on discord to debug.

| Channel | Version | Working | Notes   |
| ------- | ------- | ------- | ------- |
| ESR     | 78.5.0  | Yes (✓) | N/A     |
| Stable  | 83.0.0  | Yes (✓) | Sandbox |

| OS              | Working | Notes  |
| --------------- | ------- | ------ |
| Debian Unstable | Yes (✓) | N/A    |
| Arch            | Yes (✓) | N/A    |
| Void            | Yes (✓) | N/A    |
| Windows         | No (✗)  | WIP #1 |
| MacOS           | No (✗)  | WIP #2 |
